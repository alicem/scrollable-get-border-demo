[GtkTemplate (ui = "/org/example/App/window.ui")]
public class ScrollableGetBorderDemo.Window : Hdy.ApplicationWindow {
    [GtkChild]
    private Gtk.ListBox list;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        for (int i = 1; i <= 100; i++)
            list.add (new Hdy.ActionRow () {
                visible = true,
                title = @"Item $i"
            });
    }

    static construct {
        typeof (MacosScrollable).ensure ();
    }
}
